


from django.http import HttpResponse
from .models import GroceryItem
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render,redirect

# Create your views here.
def index(request):
	groceryitem_list = GroceryItem.objects.all()
	context = {
		"groceryitem_list" : groceryitem_list,
		"user" : request.user
	}
	
	return render(request, "django_practice/index.html", context)

def groceryitem(request, groceryitem_id):
	response = "You are viewing the detials of %s"
	return HttpResponse(response %groceryitem_id)

def register(request):
	user = User.objects.all()
	is_user_registered = False
	context = {
		"is_user_registered" : is_user_registered
	}

	for indiv_user in user:
		if indiv_user.username == "jacobp":
			is_user_registered = True
			return render(request, "django_practice/register.html", context)
			break;

	if is_user_registered == False:
		user = User() 
		user.username = "jacobp"
		user.first_name = "Jacob"
		user.last_name = "Pebello"
		user.email = "jacob@mail.com"
		user.set_password("jacob1234")
		user.is_staff = False
		user.is_active = True
		user.save()

		context = {
			"first_name" : user.first_name,
			"last_name" : user.last_name
		}
		return render(request, "django_practice/register.html", context)

def change_password(request):
	is_user_authenticated = False

	user = authenticate(username="jacobp", password="jacob1234")
	print(user)

	if user is not None:
		
		authenticated_user = User.objects.get(username='jacobp')
		
		authenticated_user.set_password("jacobp1")
		authenticated_user.save()
		is_user_authenticated = True
		context = {
			"is_user_authenticated": is_user_authenticated
		}
		return render(request, "django_practice/change_password.html", context)


def login_view(request):
	username = "jacobp"
	password = "jacobp1"
	user = authenticate(username=username, password=password)
	context = {
		"is_user_authenticated" : False
	}
	print(user)
	if user is not None:
		login(request, user)
		return redirect("index")
	else:
		return render(request, "django_practice/login.html", context)

def logout_view(request):
	logout(request)
	return redirect("index")